package PT2017.Homework3.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 * ConnectionFactory object, only on instance of this object is used through the entire lifecicle of the app
 * @author Manii
 *
 */
public class Connector {
	public static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	public static final String DBURL = "jdbc:mysql://localhost:3306/eshop?useSSL=false";
	public static String user = "";
	public static String password = "";
	
	
	private static Connector singleInstance = new Connector();
	
	private Connector(){
		try{
			Class.forName(DRIVER);//initialise the Driver class
		}
		catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}
    //method to set the user for the database  
	public static void  setUser(String userr){
		user = userr;
	}
    
	//method to set the password for the database
	public static void setPassword(String passwordd){
		password = passwordd;
	}
	
	
	private Connection createConnection(){
		
		Connection con = null;
		try{
			con = DriverManager.getConnection(DBURL,user,password);
		}
		catch(SQLException sqlException){
			sqlException.printStackTrace();
		}
		return con;
	}
	
	//method that will obtain a connection object
	public static Connection getConnection(){
		return singleInstance.createConnection();
	}
	
	//method to close the connection
	public static void closeConnection(Connection con){
		try{
			con.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	//method to close the statement
	public static void closeStatement(Statement stmt){
			try{
				stmt.close();
			}
			catch(SQLException e){
				e.printStackTrace();
			}
	}
	
	//method to close the result set
	public static void closeResultSet(ResultSet res){
		try{
			res.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}	
	
}
