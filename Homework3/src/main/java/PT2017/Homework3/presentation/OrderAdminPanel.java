package PT2017.Homework3.presentation;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class OrderAdminPanel {
	public Pane pane;
	public Scene scene;
	public Label l0;
	public Label l1;
	public Label l2;
	public Label l3;
	public Label address;
	public Label error;
	public TextField t1;
	public TextField t2;
	public ComboBox cb1;
	public ComboBox cb2;
	public Button b1;
	public Button b2;
	
	public OrderAdminPanel(){
		pane = new Pane();
		scene = new Scene(pane, 300, 500);
		
		l0 = new Label("Order administartion");
		l0.setLayoutX(100);
		l0.setLayoutY(50);
		pane.getChildren().add(l0);
		
		l1 = new Label("Product:");
		l1.setLayoutX(50);
		l1.setLayoutY(100);
		pane.getChildren().add(l1);
		
		cb1 = new ComboBox<String>();
		cb1.setLayoutX(50);
		cb1.setLayoutY(130);
		cb1.setPrefSize(200, 20);
		pane.getChildren().add(cb1);
		
		l2 = new Label("Amount:");
		l2.setLayoutX(50);
		l2.setLayoutY(170);
		pane.getChildren().add(l2);
		
		t1 = new TextField();
		t1.setLayoutX(50);
		t1.setLayoutY(200);
		t1.setPrefSize(200, 20);
		pane.getChildren().add(t1);
		
		l3 = new Label("Customer:");
		l3.setLayoutX(50);
		l3.setLayoutY(230);
		pane.getChildren().add(l3);
		
		cb2 = new ComboBox<String>();
		cb2.setLayoutX(50);
		cb2.setLayoutY(260);
		cb2.setPrefSize(200, 20);
		pane.getChildren().add(cb2);
		
		address = new Label("Address");
		address.setLayoutX(50);
		address.setLayoutY(300);
		pane.getChildren().add(address);
		
		t2 = new TextField();
		t2.setLayoutX(50);
		t2.setLayoutY(330);
		t2.setPrefSize(200, 20);
		pane.getChildren().add(t2);
		
		
		
		b1 = new Button("Place Order");
		b1.setLayoutX(110);
		b1.setLayoutY(380);
		pane.getChildren().add(b1);
		
		b2 = new Button("Back");
		b2.setLayoutX(130);
		b2.setLayoutY(420);
		pane.getChildren().add(b2);
		
		error = new Label("Under Stock");
		error.setLayoutX(50);
		error.setLayoutY(450);
		error.setVisible(false);
		pane.getChildren().add(error);
		pane.setStyle("-fx-background-color: #fae596;-fx-font-weight: bold");
		
	
	}
}
