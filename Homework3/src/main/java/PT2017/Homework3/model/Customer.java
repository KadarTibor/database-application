package PT2017.Homework3.model;
/**
 * This class models the DataBaseObject Customer
 *  it has several fields such as: id, name and e-mail;
 * @author Manii
 *
 */
public class Customer extends DataBaseObject {
	private int customer_id;
	private String name;
	private String email;
	
	public Customer(int id, String name, String email){
		this.customer_id = id;
		this.name = name;
		this.email = email;
	}
	
	public Customer(){
		this.customer_id = 0;
		this.name = "";
		this.email = "";
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String toString(){
		return name;
	}
}
