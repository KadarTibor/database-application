package PT2017.Homework3.BusinessLevel;

import java.util.List;

import PT2017.Homework3.databaseAccess.AbstractDAO;
import PT2017.Homework3.databaseAccess.Dao;
import PT2017.Homework3.model.Customer;
import PT2017.Homework3.model.DataBaseObject;
import PT2017.Homework3.model.Product;

public class ProductBusinessLogic {
private AbstractDAO<DataBaseObject> dao;
	
	public ProductBusinessLogic(Dao dao){
		this.dao = dao;
	}
	
	public boolean insertProduct(Product newProduct){
		return dao.insert(newProduct);
	}
	
	public boolean deleteProduct(Product newProduct){
		return dao.delete(newProduct);
	}
	
	public boolean updateProduct(Product newProduct,String field, String newValue){
		return dao.update(newProduct, field, newValue);
	}
	
	public Product searchProduct(String tableName,String field,String value){
		return (Product) dao.search(tableName, field, value);
	}
	
	public List<DataBaseObject> findAllProducts(){
		return dao.findAll("Product");
	}
}
