package PT2017.Homework3.presentation;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class ClientAdminPanel {
		public Pane pane;
		public Scene scene;
		public Label l1;
		public Label l2;
		public Label l3;
		public Label l4;
		public Label l5;
		public Label l6;
		public TextField t1;
		public TextField t2;
		public TextField t3;
		public Button b1;
		public Button b2;
		public Button b3;
		public Button b4;
		public TableView table;
		public Button loadTable;
		
	public ClientAdminPanel(){
		pane = new Pane();
		scene = new Scene(pane,1000,500);
		
		l1 = new Label("Customer administration");
		l1.setLayoutX(450);
		l1.setLayoutY(50);
		l1.setStyle("-fx-font-weight: bold");
		pane.getChildren().add(l1);	
		
		l2 = new Label("Customer id:");
		l2.setLayoutX(50);
		l2.setLayoutY(100);
		l2.setStyle("-fx-font-weight: bold");
		pane.getChildren().add(l2);
		
		t1 = new TextField();
		t1.setLayoutX(50);
		t1.setLayoutY(120);
		t1.setPrefSize(160, 20);
		pane.getChildren().add(t1);
		
		l3 = new Label("Customer Name:");
		l3.setLayoutX(50);
		l3.setLayoutY(150);
		pane.getChildren().add(l3);
		
		t2 = new TextField();
		t2.setLayoutX(50);
		t2.setLayoutY(170);
		t2.setPrefSize(160, 20);
		pane.getChildren().add(t2);
		
		l4 = new Label("Customer e-mail:");
		l4.setLayoutX(50);
		l4.setLayoutY(200);
		pane.getChildren().add(l4);
		
		t3 = new TextField();
		t3.setLayoutX(50);
		t3.setLayoutY(220);
		t3.setPrefSize(160, 20);
		pane.getChildren().add(t3);
		
		b1 = new Button("Insert");
		b1.setLayoutX(50);
		b1.setLayoutY(260);
		pane.getChildren().add(b1);
	
		b2 = new Button("Update");
		b2.setLayoutX(100);
		b2.setLayoutY(260);
		pane.getChildren().add(b2);
		
		b3 = new Button("Delete");
		b3.setLayoutX(160);
		b3.setLayoutY(260);
		pane.getChildren().add(b3);
		
		b4 = new Button("Back");
		b4.setLayoutX(150);
		b4.setLayoutY(400);
		pane.getChildren().add(b4);
		
		loadTable = new Button("Load Table");
		loadTable.setLayoutX(500);
		loadTable.setLayoutY(100);
		pane.getChildren().add(loadTable);
		
		l6 = new Label();
		l6.setLayoutX(100);
		l6.setLayoutY(450);
		pane.getChildren().add(l6);
		
		table = new TableView();
		table.setLayoutX(500);
		table.setLayoutY(125);
		table.setPrefHeight(150);
		table.setVisible(false);
		pane.getChildren().add(table);
		pane.setStyle("-fx-background-color: #fae596;-fx-font-weight: bold");
	}
}
