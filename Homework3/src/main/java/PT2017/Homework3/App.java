package PT2017.Homework3;
import PT2017.Homework3.presentation.*;

import PT2017.Homework3.connection.Connector;
import PT2017.Homework3.databaseAccess.AbstractDAO;
import PT2017.Homework3.databaseAccess.Dao;
import PT2017.Homework3.model.DataBaseObject;
import PT2017.Homework3.model.Order;
import PT2017.Homework3.model.Product;
import javafx.application.Application;
import javafx.stage.Stage;

public class App extends Application {
	
    public static void main( String[] args )
    {
    	launch();
//       AbstractDAO<DataBaseObject> a = new Dao();
//       //AbstractDAO<Order> b = new OrderDAO();
//       Product prod = new Product(1,"gu",2,2);
//       Order order = new Order(1,1,1);
//       //Connector.setPassword("a2a8dcd");
//       //Connector.setUser("root");
//       Product abc = (Product) a.search("Product","name","guu");
//       System.out.println(abc.toString());
//       //a.update(order,"order_id","2");
    }

	@Override
	public void start(Stage primaryStage) throws Exception {
		
			primaryStage.setTitle("Database Application");
			primaryStage.setResizable(false);
			LoginPanel logPan = new LoginPanel();
			MenuPanel menPan = new MenuPanel();
			ClientAdminPanel cpane = new ClientAdminPanel();
			ProductAdminPanel ppane = new ProductAdminPanel();
			OrderAdminPanel opane = new OrderAdminPanel();
			Controller controller = new Controller(logPan,menPan,cpane,ppane,opane,primaryStage);
			//primaryStage.setScene(cpane.scene);
			primaryStage.show();
			
	}	
		
	
}
