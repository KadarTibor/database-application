package PT2017.Homework3.presentation;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import PT2017.Homework3.BusinessLevel.CustomerBusinessLogic;
import PT2017.Homework3.BusinessLevel.OrderBusinessLogic;
import PT2017.Homework3.BusinessLevel.ProductBusinessLogic;
import PT2017.Homework3.connection.Connector;
import PT2017.Homework3.databaseAccess.Dao;
import PT2017.Homework3.model.Customer;
import PT2017.Homework3.model.DataBaseObject;
import PT2017.Homework3.model.Order;
import PT2017.Homework3.model.OrderDetails;
import PT2017.Homework3.model.Product;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class Controller implements EventHandler<ActionEvent>{
    public LoginPanel logPan;
    public MenuPanel mPan;
    public ClientAdminPanel cPan;
    public ProductAdminPanel pPan;
    public OrderAdminPanel oPan;
    public Stage primaryStage;
	public CustomerBusinessLogic cbl;
	public ProductBusinessLogic pbl;
	public OrderBusinessLogic obl;
	public Customer focusedCustomer;
    public Product focusedProduct; 
	
    public Controller(LoginPanel logPan, MenuPanel mPan, ClientAdminPanel cPan, ProductAdminPanel pPan,OrderAdminPanel oPan,Stage primaryStage) {
		this.logPan = logPan;
		this.mPan = mPan;
		this.cPan = cPan;
		this.pPan = pPan;
		this.oPan = oPan;
		this.primaryStage = primaryStage;
		primaryStage.setScene(logPan.scene);
		
		logPan.t2.textProperty().addListener( new ChangeListener<String>() {
	        @Override
	        public void changed(ObservableValue<? extends String> observable,
	                            String oldValue, String newValue) {
	            if(newValue.equals("a2a8dcd") && logPan.t1.getText().equals("root")){
	            	primaryStage.setScene(mPan.scene);
	            	Connector.setPassword("a2a8dcd");
	            	Connector.setUser("root");
	            }
	        }
		});
		
		mPan.b1.setOnAction(this);
		mPan.b2.setOnAction(this);
		mPan.b3.setOnAction(this);
		mPan.b4.setOnAction(this);
		cPan.b4.setOnAction(this);
		pPan.b4.setOnAction(this);
		oPan.b2.setOnAction(this);
		cPan.loadTable.setOnAction(this);
		pPan.loadTable.setOnAction(this);
		cPan.table.setOnMousePressed(new EventHandler<MouseEvent>() {
		    @Override 
		    public void handle(MouseEvent event) {
		        if (event.getClickCount() == 2) {
		        	focusedCustomer = (Customer) cPan.table.getSelectionModel().getSelectedItem();
					cPan.t1.setText(focusedCustomer.getCustomer_id()+"");
					cPan.t2.setText(focusedCustomer.getName());
					cPan.t3.setText(focusedCustomer.getEmail());                  
		        }
		    }
		});
		pPan.table.setOnMousePressed(new EventHandler<MouseEvent>() {
		    @Override 
		    public void handle(MouseEvent event) {
		        if (event.getClickCount() == 2) {
		        	focusedProduct = (Product) pPan.table.getSelectionModel().getSelectedItem();
					pPan.t1.setText(focusedProduct.getProduct_id()+"");
					pPan.t2.setText(focusedProduct.getName());
					pPan.t3.setText(focusedProduct.getPrice()+"");                  
					pPan.t4.setText(focusedProduct.getStock()+"");
		        }
		    }
		});
		cPan.b1.setOnAction(this);
		cPan.b2.setOnAction(this);
		cPan.b3.setOnAction(this);
		pPan.b1.setOnAction(this);
		pPan.b2.setOnAction(this);
		pPan.b3.setOnAction(this);
		oPan.b1.setOnAction(this);
		Dao dao = new Dao();
		cbl = new CustomerBusinessLogic(dao);
		pbl = new ProductBusinessLogic(dao);
		obl = new OrderBusinessLogic(dao);
		
    }

    

	@Override
	public void handle(ActionEvent event) {
		//enter in client administrator pane
		if(event.getSource()==mPan.b1){
			primaryStage.setScene(cPan.scene);
			primaryStage.centerOnScreen();
			createTable(cPan.table,cbl.findAllCustomers());
			cPan.table.setVisible(true);
		}
		//enter in product administrator pane
		if(event.getSource()==mPan.b2){
			primaryStage.setScene(pPan.scene);
			primaryStage.centerOnScreen();
			createTable(pPan.table,pbl.findAllProducts());
			pPan.table.setVisible(true);
		}
		//enter in order adiminstrator pane
		if(event.getSource()==mPan.b3){
			primaryStage.setScene(oPan.scene);
			primaryStage.centerOnScreen();
			createComboBox(oPan.cb1,pbl.findAllProducts());
			createComboBox(oPan.cb2,cbl.findAllCustomers());
		}
		//exit the application
		if(event.getSource()==mPan.b4){
			System.exit(0);
		}
		//get back to the menu
		if(event.getSource()==cPan.b4){
			primaryStage.setScene(mPan.scene);
			primaryStage.centerOnScreen();
		}
		//get back to the menu
		if(event.getSource()==pPan.b4){
			primaryStage.setScene(mPan.scene);
			primaryStage.centerOnScreen();
		}
		//get back to the menu
		if(event.getSource()==oPan.b2){
			primaryStage.setScene(mPan.scene);
			primaryStage.centerOnScreen();
		}
		//load table values from customers table
		if(event.getSource()==cPan.loadTable){
			createTable(cPan.table,cbl.findAllCustomers());
			cPan.table.setVisible(true);
		}
		//load table values from products table
		if(event.getSource()==pPan.loadTable){
			createTable(pPan.table,pbl.findAllProducts());
			pPan.table.setVisible(true);
		}
		//insert new client into the datamaze
		if(event.getSource()==cPan.b1){
			clearErrors();
			Customer newCustomer = new Customer(Integer.parseUnsignedInt(cPan.t1.getText()),cPan.t2.getText(),cPan.t3.getText());
			if(!cbl.insertCustomer(newCustomer)){
				cPan.l6.setText("Error while inserting a new customer! The customer might be already in the database");
			}
			createTable(cPan.table,cbl.findAllCustomers());
			cPan.t1.setText("");
			cPan.t2.setText("");
			cPan.t3.setText("");
			
		}
		//update the clients info.. can only update name and email
		if(event.getSource()==cPan.b2){
			clearErrors();
			cbl.updateCustomer(focusedCustomer,"name",cPan.t2.getText());
			focusedCustomer = cbl.searchCustomer("Customer","customer_id",cPan.t1.getText());
			cbl.updateCustomer(focusedCustomer,"email",cPan.t3.getText());
			createTable(cPan.table,cbl.findAllCustomers());
			cPan.t1.setText("");
			cPan.t2.setText("");
			cPan.t3.setText("");
			
		}
		//delete a customer from the table
		if(event.getSource()==cPan.b3){
			clearErrors();
			if(!cbl.deleteCustomer(focusedCustomer)){
				cPan.l6.setText("Error deleting a customer! Customer might have placed an order.");
			}
			createTable(cPan.table,cbl.findAllCustomers());
			cPan.t1.setText("");
			cPan.t2.setText("");
			cPan.t3.setText("");
		}
		//insert a product into the table
		if(event.getSource()==pPan.b1){
			clearErrors();
			Product newProduct = new Product(Integer.parseInt(pPan.t1.getText()),pPan.t2.getText(),Float.parseFloat(pPan.t3.getText()),Integer.parseInt(pPan.t4.getText()));
			if(!pbl.insertProduct(newProduct)){
				pPan.l6.setText("Error while inserting a product! Product might be already in the database");
			}
			createTable(pPan.table,pbl.findAllProducts());
			pPan.t1.setText("");
			pPan.t2.setText("");
			pPan.t3.setText("");
			pPan.t4.setText("");
		}
		//update a product form the datamaze
		if(event.getSource()==pPan.b2){
			clearErrors();
			pbl.updateProduct(focusedProduct, "name", pPan.t2.getText());
			focusedProduct =pbl.searchProduct("Product","product_id",pPan.t1.getText());
			pbl.updateProduct(focusedProduct, "price", pPan.t3.getText());
			focusedProduct =pbl.searchProduct("Product","product_id",pPan.t1.getText());
			pbl.updateProduct(focusedProduct, "stock", pPan.t4.getText());
			createTable(pPan.table,pbl.findAllProducts());
			pPan.t1.setText("");
			pPan.t2.setText("");
			pPan.t3.setText("");
			pPan.t4.setText("");
		}
		//delete a product from the datamaze
		if(event.getSource()==pPan.b3){
			clearErrors();
			if(!pbl.deleteProduct(focusedProduct)){
				pPan.l6.setText("Error deleting a product! Product might be in an order");
			}
			createTable(pPan.table,pbl.findAllProducts());
			pPan.t1.setText("");
			pPan.t2.setText("");
			pPan.t3.setText("");
			pPan.t4.setText("");
		}
		if(event.getSource()==oPan.b1){
			if(oPan.error.isVisible())
				oPan.error.setVisible(false);
			//place order only if the requirements are met
			if(!oPan.address.getText().equals("") && !oPan.t1.getText().equals("")){
				//check the stock and the amount
				Product prod =(Product) oPan.cb1.getSelectionModel().getSelectedItem();
				Customer cust = (Customer) oPan.cb2.getSelectionModel().getSelectedItem();
				if(prod.getStock() >= Integer.parseInt(oPan.t1.getText())){
					int index = obl.getLastIndex();
					Order order = new Order(index+1,prod.getProduct_id(),cust.getCustomer_id());
					obl.insertOrder(order);
					OrderDetails orderDetails = new OrderDetails(index+1,Integer.parseInt(oPan.t1.getText()),oPan.t2.getText());
					obl.insertOrderDetails(orderDetails);
					int stockleft = prod.getStock()-Integer.parseInt(oPan.t1.getText());
					pbl.updateProduct(prod, "stock",""+stockleft);
					
					Document document = new Document();
				      try
				      {
				         PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Order_"+ orderDetails.getOrder_id() +".pdf"));
				         document.open();
				         //call the paragraph creator method
				         document.add(createPDFText(cust,prod,orderDetails));
				         document.close();
				         writer.close();
				      } catch (DocumentException e)
				      {
				         e.printStackTrace();
				      } catch (FileNotFoundException e)
				      {
				         e.printStackTrace();
				      }
				      oPan.t1.setText("");
				      oPan.t2.setText("");
					
				}
				else{
					//dsplay the under stock messege
					oPan.error.setVisible(true);
				}
			}
		}
	}
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private void createTable(TableView table,List<? extends DataBaseObject> objects){
    	ObservableList<DataBaseObject> data = FXCollections.observableArrayList(objects);
    	if(table.getItems().isEmpty()){
	    	Field[] fields = objects.get(0).getClass().getDeclaredFields();
	    	List<TableColumn> cols = new ArrayList<TableColumn>();
	    	for(Field fieldes:fields){
	    		fieldes.setAccessible(true);
	    		TableColumn newCol = new TableColumn(fieldes.getName());
	    		newCol.setMinWidth(100);
	    		newCol.setCellValueFactory(new PropertyValueFactory<>(fieldes.getName()));
	    		cols.add(newCol);
	    	}
			table.setPrefWidth(100*fields.length);
	    	table.setEditable(true);
	    	table.getColumns().addAll(cols);
	    	table.setItems(data);
	    	
    	}
    	else{
    		data.clear();
    		data.addAll(objects);
    		table.setItems(data);
    	}
    	
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private void createComboBox(ComboBox comboBox,List<? extends DataBaseObject> objects){
    	ObservableList<DataBaseObject> data = FXCollections.observableArrayList(objects);
    	comboBox.setItems(data);
    	comboBox.getSelectionModel().selectFirst();
    }
    
    private Paragraph createPDFText(Customer cust,Product prod, OrderDetails orderDetails){
    	StringBuilder text = new StringBuilder();
    	text.append("ORDER NUMBER " + orderDetails.getOrder_id());
    	text.append("\nOrder was made by customer: "+ cust.getName());
    	text.append("\nProduct: "+ prod.getName());
    	text.append("\nQuantity: "+ orderDetails.getQuantity());
    	text.append("\nAddress: "+orderDetails.getAddress());
    	text.append("\n------------------------------\n");
    	float total = prod.getPrice()*orderDetails.getQuantity();
    	text.append("\nTOTAL "+ total);
    	return new Paragraph(text.toString());
    }
    
    private void clearErrors(){
    	cPan.l6.setText("");
    	pPan.l6.setText("");
    }
}
