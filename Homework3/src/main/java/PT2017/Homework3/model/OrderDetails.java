package PT2017.Homework3.model;

/**
 * This class models the DataBaseObject OrderDetals
 *  it has several fields such as: order_id, address, quantity ordered;
 * @author Manii
 *
 */
public class OrderDetails extends DataBaseObject {
 
  private int quantity;
  private String address;
  private int order_id;
  
  
	public OrderDetails(int order_id, int quantity, String address) {
		this.order_id = order_id;
		this.quantity = quantity;
		this.address = address;
	}
	public OrderDetails() {
		this.order_id = 0;
		this.quantity = 0;
		this.address = "";
	}

	public int getOrder_id() {
		return order_id;
	}


	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}
}
