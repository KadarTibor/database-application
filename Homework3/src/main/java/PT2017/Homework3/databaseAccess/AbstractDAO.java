package PT2017.Homework3.databaseAccess;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import PT2017.Homework3.connection.Connector;
import PT2017.Homework3.model.Customer;
import PT2017.Homework3.model.Order;
import PT2017.Homework3.model.OrderDetails;
import PT2017.Homework3.model.Product;
/**
 * This class makes a bridge between the requested operation and the actual transaction with the database
 * The class takes as a parameter a database object such as Customer, Product, Order or OrderDetails
 * The class uses reflection on the received object and constructs a personalized transaction request
 * @author Manii
 *
 * @param <T>
 */
public class AbstractDAO<T>{
      
	@SuppressWarnings("rawtypes")
	public Class type;
      
    public AbstractDAO(){
    	//this.type = (Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    
    /**
     * This method will create an insert query based on the type of the attribute type
     * @return String that is recommended to be used with a PreparedStatement
     */
    private String insertQuery(){
    	StringBuilder insert = new StringBuilder();
    	insert.append("INSERT ");
    	insert.append("INTO ");
    	insert.append("eshop."+type.getSimpleName());
    	insert.append(" VALUES ");
       	Field[] fields = type.getDeclaredFields();
    	insert.append(" ( ");
       	for(int i = 0; i < fields.length - 1; i++){
    		insert.append(" ?,");
    	}
       	insert.append(" ?)");
    	return insert.toString();
    }
    
    /**
     * will insert any kind of object into the database given the database knows about such an object
     * @param object DataBaseObject
     */
    public boolean insert(T object){
    	//this line ensures that i will have different object everytim
    	this.type = object.getClass();
    	Connection connection = null;
    	PreparedStatement stmt = null;
    	String query = insertQuery();
    	try{
    		connection = Connector.getConnection();
    		stmt = connection.prepareStatement(query);
    		System.out.println(stmt.toString());
    		
    		Field[] fields = type.getDeclaredFields();
    		for(int i = 1; i <= fields.length; i++ ){
    			String value = "";
    			try{
    				fields[i-1].setAccessible(true);;
    				value = fields[i-1].get(object).toString();
    				System.out.println(value);
    			}
    			catch(IllegalArgumentException | IllegalAccessException  e){
    				e.printStackTrace();
    			}
    			if(fields[i-1].getType().toString().equals("int")){
    				stmt.setInt(i,Integer.valueOf(value));
    			}
    			if(fields[i-1].getType().toString().equals("class java.lang.String")){
    				stmt.setString(i, value);
    			}
    			if(fields[i-1].getType().toString().equals("float")){
    				stmt.setFloat(i, Float.valueOf(value));
    			}
    		}
    		System.out.println(stmt.toString());
    		stmt.executeUpdate();
    		return true;
    	}
    	catch(SQLException e){
    		e.printStackTrace();
    		return false;
    	}
    	finally{
    		Connector.closeConnection(connection);
    		Connector.closeStatement(stmt);
    	}
    }
    
    //delete object
    private String deleteQuery(){
    	StringBuilder delete = new StringBuilder();
    	delete.append("DELETE ");
    	delete.append("FROM ");
    	delete.append("eshop."+type.getSimpleName());
    	delete.append(" WHERE ");
    	Field[] fields = type.getDeclaredFields();
    	delete.append(" ( ");
       	for(int i = 0; i < fields.length - 1; i++){
    		delete.append( fields[i].getName() + "= ? and " );
    	}
       	delete.append(fields[fields.length-1].getName() + "= ?)");
    	return delete.toString();
    }
    
    
    /**
     * will delete any kind of object from the database given the database knows about such an object
     * @param object DataBaseObject
     */
    public boolean delete(T object){
    	type = object.getClass();
    	Connection connection = null;
    	PreparedStatement stmt = null;
    	String query = deleteQuery();
    	try{
    		connection = Connector.getConnection();
    		stmt = connection.prepareStatement(query);
    		System.out.println(stmt.toString());
    		
    		Field[] fields = type.getDeclaredFields();
    		for(int i = 1; i <= fields.length; i++ ){
    			String value = "";
    			try{
    				fields[i-1].setAccessible(true);;
    				value = fields[i-1].get(object).toString();
    				System.out.println(value);
    			}
    			catch(IllegalArgumentException | IllegalAccessException  e){
    				e.printStackTrace();
    			}
    			if(fields[i-1].getType().toString().equals("int")){
    				stmt.setInt(i,Integer.valueOf(value));
    			}
    			if(fields[i-1].getType().toString().equals("class java.lang.String")){
    				stmt.setString(i, value);
    			}
    			if(fields[i-1].getType().toString().equals("float")){
    				stmt.setFloat(i, Float.valueOf(value));
    			}
    		}
    		System.out.println(stmt.toString());
    		stmt.executeUpdate();
    		return true;
    	}
    	catch(SQLException e){
    		e.printStackTrace();
    		return false;
    	}
    	finally{
    		Connector.closeConnection(connection);
    		Connector.closeStatement(stmt);
    	}
    }
    
    //edit object
    private String updateQuery(String field){
    	StringBuilder update = new StringBuilder();
    	update.append("UPDATE ");
    	update.append("eshop." + type.getSimpleName());
    	update.append(" SET " + field + " = ? ");
    	update.append("WHERE ");
    	Field[] fields = type.getDeclaredFields();
    	update.append(" ( ");
       	for(int i = 0; i < fields.length - 1; i++){
    		update.append( fields[i].getName() + "= ? and " );
    	}
       	update.append(fields[fields.length-1].getName() + "= ?)");
    	return update.toString();
    }
    /**
     * will update any kind of object into the database given the database knows about such an object
     * @param object DataBaseObject
     * @param String field -- which field to be updated
     * @param String newValue -- the new value to be inserted into the field field speciefied earlier, the type of this newValue is decided internally to match the request
     */
    
    public boolean update(T object,String field,String newValue){
    	type = object.getClass();
    	Connection connection = null;
    	PreparedStatement stmt = null;
    	String query = updateQuery(field);
    	try{
    		connection = Connector.getConnection();
    		stmt = connection.prepareStatement(query);
    		System.out.println(stmt.toString());
    		Field[] fields = type.getDeclaredFields();
    		for(int i = 0; i < fields.length; i++){
		    		try{
		    			fields[i].setAccessible(true);
		    			System.out.println(fields[i].getName());
			    		if(fields[i].getName().equals(field)){
			    			if(fields[i].getType().equals("int"))
				    				stmt.setInt(1, Integer.valueOf(newValue));
			    			if(fields[i].getType().toString().equals("float")){
			    				stmt.setFloat(1, Float.valueOf(newValue));
			    			}
			    			else
				    				stmt.setString(1, newValue);
			    		}
		    		}
		    		catch(Exception e){
		    			e.printStackTrace();
		    		}
    		}
    		for(int i = 1; i <= fields.length; i++ ){
    			String value = "";
    			try{
    				fields[i-1].setAccessible(true);;
    				value = fields[i-1].get(object).toString();
    				System.out.println(value);
    			}
    			catch(IllegalArgumentException | IllegalAccessException  e){
    				e.printStackTrace();
    			}
    			if(fields[i-1].getType().toString().equals("int")){
    				stmt.setInt(i + 1,Integer.valueOf(value));
    			}
    			if(fields[i-1].getType().toString().equals("class java.lang.String")){
    				stmt.setString(i + 1, value);
    			}
    			if(fields[i-1].getType().toString().equals("float")){
    				stmt.setFloat(i + 1, Float.valueOf(value));
    			}
    		}
    		System.out.println(stmt.toString());
    		stmt.executeUpdate();
    		return true;
    	}
    	catch(SQLException e){
    		e.printStackTrace();
    		return false;
    	}
    	finally{
    		Connector.closeConnection(connection);
    		Connector.closeStatement(stmt);
    	}
    }
    //find object
    private String selectQuery(String tableName, String field){
    	StringBuilder select = new StringBuilder();
    	select.append("SELECT ");
    	select.append("* ");
    	select.append("FROM ");
    	select.append("eshop." + tableName);
    	select.append(" Where " + field + " = ?");
    	return select.toString();
    }
    
    
    /**
     * will search any kind of object into the database given the database knows about such an object
     * @param object DataBaseObject
     * @param String field- table attribute by which to search
     * @param String searched value
     */
    public T search(String tableName,String field,String value){
    	Connection connection = null;
    	PreparedStatement stmt = null;
    	ResultSet rset = null;
    	String query = selectQuery(tableName,field);
    	
    	if(tableName.equals("Product"))
    		type = new Product().getClass();
    	if(tableName.equals("Order"))
    		type = new Order().getClass();
    	if(tableName.equals("Customer"))
    		type = new Customer().getClass();
    	if(tableName.equals("OrderDetails"))
    		type  = new OrderDetails().getClass();
    	try{
    		connection = Connector.getConnection();
    		stmt = connection.prepareStatement(query);
    		System.out.println(stmt.toString());
    		Field[] fields = type.getDeclaredFields();
    		for(Field fildes: fields){
    			if(fildes.getName().equals(field)){
    				if(fildes.getType().equals("int"))
    					stmt.setInt(1, Integer.valueOf(value));
    				if(fildes.getType().toString().equals("float")){
        				stmt.setFloat(1, Float.valueOf(value));
        			}
    				else
    					stmt.setString(1, value);
    			}
    		}
    		System.out.println(stmt);
    		rset = stmt.executeQuery();
    		return createObject(rset).get(0);
    	}
    	catch(SQLException e){
    		e.printStackTrace();
    	}
    	finally{
    		Connector.closeConnection(connection);
    		Connector.closeStatement(stmt);
    		Connector.closeResultSet(rset);
    	}
    	return null;
    }
    
    /**
     * This method uses reflection to create any kind of database object
     * @param rset - result set of database rows
     * @return a list of objects 
     */
    private List<T> createObject(ResultSet rset){
    	List<T> objects = new ArrayList<T>();
    	try {
			
			while(rset.next()){
				@SuppressWarnings("unchecked")
				T instance = (T) type.newInstance();
				for(Field field: type.getDeclaredFields()){
					Object value = rset.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(),type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance,value);	
				}
			objects.add(instance);
			}
			return objects;
			
		} catch (InstantiationException e) {
			
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		} catch (IntrospectionException e) {
			
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			
			e.printStackTrace();
		}
    	return null;
    }
    
    //find all in a table
    private String selectAll(String tableName){
    	StringBuilder select = new StringBuilder();
    	select.append("SELECT * ");
    	select.append("FROM ");
    	select.append("eshop."+tableName);
    	return select.toString();
    }
    
    /**
     * 
     * @param tableName
     * @return list of all the objects from a table
     */
    public List<T> findAll(String tableName){
    	Connection connection = null;
    	PreparedStatement stmt = null;
    	ResultSet rset = null;
    	String query = selectAll(tableName);
    	if(tableName.equals("Product"))
    		type = new Product().getClass();
    	if(tableName.equals("Order"))
    		type = new Order().getClass();
    	if(tableName.equals("Customer"))
    		type = new Customer().getClass();
    	if(tableName.equals("OrderDetails"))
    		type  = new OrderDetails().getClass();
    	try{
    		connection = Connector.getConnection();
    		stmt = connection.prepareStatement(query);
    		System.out.println(stmt.toString());
    		rset = stmt.executeQuery();
    		return createObject(rset);
    	}
    	catch(SQLException e){
    		e.printStackTrace();
    	}
    	return null;
    }
    
    /**
     * 
     * @param tableName
     * @return the index of the last object in the table
     */
    public int getlastIndex(String tableName){
    	Connection connection = null;
    	PreparedStatement stmt = null;
    	ResultSet rset = null;
    	String query = selectAll(tableName);
    	try{
    		connection = Connector.getConnection();
    		stmt = connection.prepareStatement(query);
    		System.out.println(stmt.toString());
    		rset = stmt.executeQuery();
    		int counter = 0;
    		while(rset.next())
    			counter++;
    		return counter;
    	}
    	catch(SQLException e){
    		e.printStackTrace();
    	}
    	return 0;
    }
}
