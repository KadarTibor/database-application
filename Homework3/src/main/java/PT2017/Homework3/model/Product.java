package PT2017.Homework3.model;
/**
 * This class models the DataBaseObject Product
 *  it has several fields such as: id, name, price and stock;
 * @author Manii
 *
 */
public class Product extends DataBaseObject {
   private int product_id;
   private String name;
   private float price;
   private int stock;

   public Product(int product_id, String name, float price, int stock) {
	   
	this.product_id = product_id;
	this.name = name;
	this.price = price;
	this.stock = stock;
   }
   
   public Product() {
	   
		this.product_id = 0;
		this.name = "";
		this.price = 0;
		this.stock = 0;
	   }

	public int getProduct_id() {
		return product_id;
	}
	
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public float getPrice() {
		return price;
	}
	
	public void setPrice(float price) {
		this.price = price;
	}
	
	public int getStock() {
		return stock;
	}
	
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	public String toString(){
		return name; 
	}
}
