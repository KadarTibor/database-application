package PT2017.Homework3.presentation;

import javax.swing.event.ChangeListener;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class LoginPanel {
	public Label l1;
	public Label l2;
	public TextField t1;
	public PasswordField t2;
	public Button b1;
	public Pane pane;
	public Scene scene;
	
	public LoginPanel(){
	
		pane = new Pane();
		scene = new Scene(pane, 300, 300);
		
		l1 = new Label("User");
		l1.setLayoutX(75);
		l1.setLayoutY(75);
		l1.setStyle("-fx-font-weight: bold");
		pane.getChildren().add(l1);
		
		t1 = new TextField();
		t1.setLayoutX(75);
		t1.setLayoutY(100);
		t1.setStyle("-fx-font-weight: bold");
		t1.setPrefSize( 150,20);
		//t1.setStyle("-fx-control-inner-background: #3fb0ac");
		pane.getChildren().add(t1);
		
		l2 = new Label("Password");
		l2.setLayoutX(75);
		l2.setLayoutY(150);
		l2.setStyle("-fx-font-weight: bold");
		pane.getChildren().add(l2);
	
		t2 = new PasswordField();
		t2.setLayoutX(75);
		t2.setLayoutY(175);
		t2.setPrefSize( 150,20);
		//t2.setStyle("-fx-control-inner-background: #3fb0ac");
		pane.getChildren().add(t2);
		pane.setStyle("-fx-background-color: #fae596;");
	}
}
