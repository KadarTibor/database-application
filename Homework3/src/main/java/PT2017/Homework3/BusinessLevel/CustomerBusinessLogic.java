package PT2017.Homework3.BusinessLevel;

import java.util.List;

import PT2017.Homework3.databaseAccess.AbstractDAO;
import PT2017.Homework3.databaseAccess.Dao;
import PT2017.Homework3.model.Customer;
import PT2017.Homework3.model.DataBaseObject;

public class CustomerBusinessLogic {
	
	private AbstractDAO<DataBaseObject> dao;
	
	public CustomerBusinessLogic(Dao dao){
		this.dao = dao;
	}
	
	public boolean insertCustomer(Customer newCustomer){
		return dao.insert(newCustomer);
	}
	
	public boolean deleteCustomer(Customer customer){
		return dao.delete(customer);
	}
	
	public boolean updateCustomer(Customer customer,String field, String newValue){
		return dao.update(customer, field, newValue);
	}
	
	public Customer searchCustomer(String tableName,String field,String value){
		return (Customer) dao.search(tableName, field, value);
	}
	
	public List<DataBaseObject> findAllCustomers(){
		return dao.findAll("Customer");
	}
}
