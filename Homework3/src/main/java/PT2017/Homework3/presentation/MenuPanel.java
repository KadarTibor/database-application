package PT2017.Homework3.presentation;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

public class MenuPanel {
     public Label l1;
     public Button b1;
     public Button b2;
     public Button b3;
     public Button b4;
     public Scene scene;
     public Pane pane;

     public MenuPanel(){
    	 
    	 pane = new Pane();
 		 scene = new Scene(pane,300,500);
 		 
 		 l1 = new Label("Greetings!");
 		 l1.setStyle("-fx-font-weight: bold");
 		 l1.setLayoutX(120);
 		 l1.setLayoutY(50);
 		 pane.getChildren().add(l1);
 		 
 		 b1 = new Button("Manage Clients");
 		 b1.setLayoutX(50);
 		 b1.setLayoutY(150);
 		 b1.setPrefSize(200, 20);
 		 pane.getChildren().add(b1);
     
 	   	 b2 = new Button("Manage Products");
		 b2.setLayoutX(50);
		 b2.setLayoutY(225);
		 b2.setPrefSize(200, 20);
		 pane.getChildren().add(b2);
		 
		 b3 = new Button("Manage Orders");
 		 b3.setLayoutX(50);
 		 b3.setLayoutY(300);
 		 b3.setPrefSize(200, 20);
 		 pane.getChildren().add(b3);
 		 
 		 b4 = new Button("Exit");
		 b4.setLayoutX(50);
		 b4.setPrefSize(200, 20);
		 b4.setLayoutY(375);
		 pane.getChildren().add(b4);
		 pane.setStyle("-fx-background-color: #fae596;-fx-font-weight: bold");
     }
}

