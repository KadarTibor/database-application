package PT2017.Homework3.model;
/**
 * This class models the DataBaseObject Order
 *  it has several fields such as: id, product id, customer id;
 * @author Manii
 *
 */
public class Order extends DataBaseObject {
	
	private int customer_id;
	private int order_id;
	private int product_id;
	
	public Order(int customer_id, int order_id, int product_id) {
		this.customer_id = customer_id;
		this.order_id = order_id;
		this.product_id = product_id;
	}
	
	public Order() {
		this.customer_id = 0;
		this.order_id = 0;
		this.product_id = 0;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public int getOrder_id() {
		return order_id;
	}

	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

}
