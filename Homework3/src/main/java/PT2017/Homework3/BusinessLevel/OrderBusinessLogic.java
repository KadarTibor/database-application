package PT2017.Homework3.BusinessLevel;



import PT2017.Homework3.databaseAccess.AbstractDAO;
import PT2017.Homework3.databaseAccess.Dao;
import PT2017.Homework3.model.DataBaseObject;
import PT2017.Homework3.model.Order;
import PT2017.Homework3.model.OrderDetails;

public class OrderBusinessLogic {
private AbstractDAO<DataBaseObject> dao;
	
	public OrderBusinessLogic(Dao dao){
		this.dao = dao;
	}
	
	public void insertOrder(Order newOrder){
		dao.insert(newOrder);
	}
	
	public void insertOrderDetails(OrderDetails newOrderDetails){
		dao.insert(newOrderDetails);
	}
	
	public int getLastIndex(){
		return dao.getlastIndex("Order");
	}
}
